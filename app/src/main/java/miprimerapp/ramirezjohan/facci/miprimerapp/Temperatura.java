package miprimerapp.ramirezjohan.facci.miprimerapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Temperatura extends AppCompatActivity {
    Button convertir=null;
    EditText cantidad=null;
    TextView resultado=null;
    Spinner spincl=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);
        convertir=(Button)findViewById(R.id.btntem);
        cantidad=(EditText)findViewById(R.id.cant);
        resultado=(TextView)findViewById(R.id.resu);
        spincl=(Spinner)findViewById(R.id.spinla);

        String[] op ={"Selecciona Una Opcion","°C a °F","°F a °C"};

        ArrayAdapter<String> adapter=new
                ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,op);
        spincl.setAdapter(adapter);

        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cantidad.getText().toString().equals("")){
                    Toast msg=Toast.makeText(getApplicationContext(),"Escribe La Cantidad",Toast.LENGTH_SHORT);
                    msg.show();

                } else {
                    Double c = Double.parseDouble(cantidad.getText().toString());
                    Double res=null;
                    int select =spincl.getSelectedItemPosition();

                    switch (select){
                        case 0:
                            res=0.0;
                            Toast.makeText(getApplicationContext(),"Selecciona Una Opcion",Toast.LENGTH_SHORT).show();
                            break;

                        case 1:
                            res=1.8 * c +32;
                            break;

                        case 2:
                            res=(c-32)/1.8;
                            break;

                    }
                     resultado.setText(res.toString());


                }

            }
        });



    }
}
